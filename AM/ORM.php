<?php

namespace AM;

class ORM
{
	public $data;

	public static $db;
	public static $table;
	public static $key = 'id';

	public function __construct($id = 0)
	{
		$this->data = array();

		if(! $id) return;

		if( is_numeric($id) )
		{
			$this->data[static::$key] = $id;
		}
		else
		{
			$this->data = (array) $id;
		}
	}
	
	public function __set( $key, $value)
	{
		$this->data[ $key ] = $value;
	}

	public function __get( $key )
	{
		if( isset( $this->data[ $key ] ) )
		{
			return $this->data[ $key];
		}
		else
		{
			return 0;
		}
	}
	
	static public function beginTransaction()
	{
		return self::$db->pdo->beginTransaction();
	}
	
	static public function commit()
	{
		return self::$db->pdo->commit();
	}
	
	static public function rollBack()
	{
		return self::$db->pdo->rollBack();
	}
	
	static public function select( $sql, $param, $return_array = false )
	{
		$time = microtime( true );
		
		\AM\Database::$last_query = $sql;
		
		if ( $stmt = self::$db->pdo->prepare( $sql ) )
		{
			$i = 1;
			foreach( $param as $value )
			{
				$stmt->bindValue( $i++, $value );
			}
			
			if ( $stmt->execute() )
			{
				if( $return_array )
				{
					$result = $stmt->fetchAll(  \PDO::FETCH_ASSOC );
				}
				else
				{
					$result = $stmt->fetchAll( \PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, get_called_class() );
				}
				\AM\Database::$queries[self::$db->type][] = array( microtime( true ) - $time, $sql);
				
				if ( is_array( $result ) )
				{
					return $result;
				}
			}
		}
		return false;
	}
	
		// INSERT, UPDATE, DELETE
	
	public static function sql( $sql, $param )
	{
		$time = microtime( true );
		
		\AM\Database::$last_query = $sql;
		
		if ( $stmt = self::$db->pdo->prepare( $sql ) )
		{
			$i = 1;
			foreach( $param as $value )
			{
				$stmt->bindValue( $i++, $value );
			}
			
			if ( $stmt->execute() )
			{
				if ( preg_match('/^INSERT/', $sql) )
				{
					/* PostgreSQL */
					$result = $stmt->fetch( \PDO::FETCH_NUM );
					$result = $result[0];
					
					/* MySql ??
					$result = self::$db->pdo->lastInsertId();
					*/ 
				}
				else
				{
					$result = $stmt->rowCount();
				}
				
				\AM\Database::$queries[ self::$db->type ][] = array( microtime( true ) - $time, $sql);
				
				return $result;
			}
			else
			{
				return false;
			}
		}	
		return false;			
	}	
	
	
	static public function row( $where, $clause = null )
	{	
		$sql = 'SELECT * FROM '.static::$table.' WHERE ';
		
		$values = array();
		
		foreach ( $where as $key => $value )
		{
			$sql .= $key.'=? AND ';
			array_push( $values, $value );
		}
		
		$sql = trim( $sql, 'AND ');
		
		if ( $clause )
		{
			$sql .= $clause;
		}
		
		$sql .= ' LIMIT 1';
		
		$rows = self::select( $sql, $values );
		
		if ( $rows )
		{
			return $rows[0];
		}
		else
		{
			return false;
		}
	}
	
	static public function column( $column, $where )
	{
		$sql = 'SELECT '.$column.' FROM '.static::$table.' WHERE ';
		$values = array();
		
		foreach ( $where as $key => $value )
		{
			$sql .= $key.'=? AND ';
			array_push( $values, $value );
		}
		
		$sql = trim( $sql, 'AND ');
		$sql .= ' LIMIT 1';
		
		$rows = self::select( $sql, $values, true );
		
		return $rows[0][$column];		
	}

	
	public static function count( $where )
	{
		$sql = 'SELECT COUNT(*) FROM '.static::$table.' WHERE ';
		$values = array();
		
		foreach ( $where as $key => $value )
		{
			$sql .= $key.'=? AND ';
			array_push( $values, $value );
		}
		
		$sql = trim( $sql, 'AND ');
	
		$tab = self::select( $sql, $values );
		
		return $tab[0]->count;
	}
	
	public function save()
	{
		if ( $this->{static::$key} ) 
		{	
			$param = array();
			$data_string = '';
			
			foreach ( $this->data as $key => $value )
			{
				if( $key == static::$key )
				{ 
					continue;
				}
				$data_string .= $key.'=?, ';
				array_push( $param, $value );
			}
			
			array_push( $param, $this->{static::$key} );
			$data_string = trim( $data_string, ', ' );
			
			$sql = 'UPDATE '.static::$table.' SET '.$data_string.' WHERE '.static::$key.' = ?';
			
			return self::sql( $sql, $param );
		}
		else
		{
			$param = array();
			$keys = '';
			$question_marks = '';
			
			foreach ( $this->data as $key => $value )
			{
				$keys .= $key.', ';
				array_push( $param, $value );
				$question_marks .= '?, '; 
			}
			
			$keys = trim( $keys, ', ' );
			$question_marks = trim( $question_marks, ', ' );
			
			$sql = 'INSERT INTO '.static::$table.' ('.$keys.') VALUES ('.$question_marks.') RETURNING '.static::$key;			
			
			return $this->{static::$key} = self::sql( $sql, $param );			
		}
	}
	
	public static function delete( $where )
	{
		$where_clause = '';
		$values = array();
		
		foreach( $where as $key => $value )
		{
			$where_clause .= ' '.$key.'=? AND';
			array_push($values, $value);
		}
		$where_clause = trim($where_clause, ' AND');
		
		return self::sql('DELETE FROM '.static::$table.' WHERE '.$where_clause, $values );
	}
	
	public function self_delete()
	{
		self::delete( array( static::$key => $this->data[ static::$key ] ) );
	}

}


